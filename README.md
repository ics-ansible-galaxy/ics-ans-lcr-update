ics-ans-lcr-update
==================

Ansible playbook to update the LCR workstations.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

License
-------

BSD 2-clause
